package com.linerobot.lucene;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.linerobot.database.handler.DatabaseHandler;
import com.linerobot.textmining.handler.TextMiningHandler;

public class IndexHandler {

	private static Logger logger = LoggerFactory.getLogger(IndexHandler.class);
	private Directory directory;
	private Analyzer analyzer;
	private TextMiningHandler miningHandler;
	private List<String> stopWords;
	// private String dictPrefix =
	// "D:/BigData/Java/Workspace/apache-tomcat-8.0.51/webapps/dictionary/";
	private String dictPrefix = "D:/Git/Java/apache-tomcat-8.0.51/webapps/dictionary/";
	// private String indexPrefix = "D:/BigData/Java/Workspace/";
	private String indexPrefix = "D:/Git/Java/";
	private String indexPath = this.indexPrefix + "apache-tomcat-8.0.51/webapps/index.lucene";
	private String stopWordsPath = this.dictPrefix + "stopword_chinese.txt";

	public IndexHandler() throws IOException {
		this.directory = FSDirectory.open(Paths.get(this.indexPath));
		this.analyzer = new StandardAnalyzer();
		this.stopWords = Files.readAllLines(Paths.get(this.stopWordsPath), Charsets.UTF_8);
		this.miningHandler = TextMiningHandler.getInstance();
	}

	public static IndexHandler getInstance() throws IOException {
		return new IndexHandler();
	}

	public void indexInputSentence(UUID uuid, String sentence, String option) throws IOException {
		IndexWriterConfig config = new IndexWriterConfig(this.analyzer);
		config.setOpenMode(OpenMode.CREATE_OR_APPEND);
		IndexWriter writer = new IndexWriter(this.directory, config);
		try {
			List<String> sentences = Arrays.asList(sentence.split("，"));
			for (String part : sentences) {
				List<String> pmiKeyword = this.miningHandler.getKeywords(part, 5, option);
				for (String keyword : pmiKeyword) {
					Document doc = new Document();
					logger.info("正在加入索引: UUID -> {}, keyword -> {}, sentence -> {}", uuid.toString(), keyword,
							sentence);
					doc.add(new TextField("uuid", uuid.toString(), Field.Store.YES));
					doc.add(new TextField("keyword", keyword, Field.Store.YES));
					doc.add(new TextField("sentence", sentence, Field.Store.YES));
					writer.addDocument(doc);
				}
			}
		} catch (IOException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (writer.isOpen()) {
				writer.close();
			}
		}
	}

	public Map<String, Map<String, String>> queryInputSentence(String sentence, String option)
			throws IOException, ParseException, ClassNotFoundException, SQLException {
		IndexReader reader = DirectoryReader.open(this.directory);
		IndexSearcher searcher = new IndexSearcher(reader);

		List<String> keywords = this.miningHandler.getKeywords(sentence, 5, option);
		String keyword = keywords.get(0);

		String[] fields = { "uuid", "keyword", "sentence" };
		QueryParser parser = new MultiFieldQueryParser(fields, analyzer);
		Query query = parser.parse(keyword);

		TopDocs topResult = searcher.search(query, 5);
		logger.info("\"{}\" found in {} document.", keyword, topResult.totalHits);

		List<ScoreDoc> results = Arrays.asList(searcher.search(query, 5).scoreDocs);
		for (ScoreDoc result : results) {
			Document foundDoc = searcher.doc(result.doc);
			logger.info("{}, uuid:{}, keyword:{}, sentence:{}", result.toString(),
					foundDoc.getField("uuid").stringValue(), foundDoc.getField("keyword").stringValue(),
					foundDoc.getField("sentence").stringValue());
		}

		if (!results.isEmpty()) {
			Document foundDoc = searcher.doc(results.get(0).doc);
			return DatabaseHandler.queryIndexData(foundDoc.getField("uuid").stringValue());
		}
		return null;
	}

	public List<String> removeStopWords(List<String> segResult) throws IOException {
		List<String> removedResult = Lists.newArrayList();
		for (String segWord : segResult) {
			Boolean isEqual = false;
			for (String stopWord : this.stopWords) {
				if (segWord.replace("(|)|（|）", "").equals(stopWord)) {
					isEqual = !isEqual;
					break;
				}
			}
			if (!isEqual) {
				removedResult.add(segWord);
				logger.info("已將去除停止詞的結果加入:{}", segWord);
			}
		}
		return removedResult;
	}
}
