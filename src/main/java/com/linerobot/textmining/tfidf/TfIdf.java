package com.linerobot.textmining.tfidf;

import java.math.BigDecimal;

public class TfIdf {

	private int serialNumber;
	private String term;
	private int wordCount;
	private BigDecimal tf;
	private int df;
	private BigDecimal idf;
	private BigDecimal tfIdf;
	private int isStopWord;

	public TfIdf() {

	}

	public TfIdf(String term, int wordCount, BigDecimal tf, int df, BigDecimal idf, BigDecimal tfIdf, int isStopWord) {
		this.term = term;
		this.wordCount = wordCount;
		this.tf = tf;
		this.df = df;
		this.idf = idf;
		this.tfIdf = tfIdf;
		this.isStopWord = isStopWord;
	}

	public TfIdf(int serialNumber, String term, int wordCount, BigDecimal tf, int df, BigDecimal idf, BigDecimal tfIdf,
			int isStopWord) {
		this.serialNumber = serialNumber;
		this.term = term;
		this.wordCount = wordCount;
		this.tf = tf;
		this.df = df;
		this.idf = idf;
		this.tfIdf = tfIdf;
		this.isStopWord = isStopWord;
	}

	public int getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getTerm() {
		return this.term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public int getWordCount() {
		return this.wordCount;
	}

	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}

	public BigDecimal getTf() {
		return this.tf;
	}

	public void setTf(BigDecimal tf) {
		this.tf = tf;
	}

	public int getDf() {
		return this.df;
	}

	public void setDf(int df) {
		this.df = df;
	}

	public BigDecimal getIdf() {
		return this.idf;
	}

	public void setIdf(BigDecimal idf) {
		this.idf = idf;
	}

	public BigDecimal getTfIdf() {
		return this.tfIdf;
	}

	public void setTfIdf(BigDecimal tfIdf) {
		this.tfIdf = tfIdf;
	}

	public int getIsStopWord() {
		return this.isStopWord;
	}

	public void setIsStopWord(int isStopWord) {
		this.isStopWord = isStopWord;
	}

	@Override
	public String toString() {
		return "TfIdf [serialNumber=" + serialNumber + ", term=" + term + ", wordCount=" + wordCount + ", tf=" + tf
				+ ", df=" + df + ", idf=" + idf + ", tfIdf=" + tfIdf + ", isStopWord=" + isStopWord + "]";
	}

}
