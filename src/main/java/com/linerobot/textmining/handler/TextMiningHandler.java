package com.linerobot.textmining.handler;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.JiebaSegmenter.SegMode;
import com.huaban.analysis.jieba.WordDictionary;
import com.linerobot.database.handler.DatabaseHandler;
import com.linerobot.textmining.tfidf.TfIdf;

public class TextMiningHandler {

	private static Logger logger = LoggerFactory.getLogger(TextMiningHandler.class);
	private JiebaSegmenter segmenter;
	private WordDictionary wordDictionary;
	// private List<String> punctuations;
	private List<String> stopWords;
	// private String prefix =
	// "D:/BigData/Java/Workspace/apache-tomcat-8.0.51/webapps/dictionary/";
	private String prefix = "D:/Git/Java/apache-tomcat-8.0.51/webapps/dictionary/";
	// private String jiebaDictPath = this.prefix + "dict.txt.big.txt";
	private String monDictPath = this.prefix + "dict_revised_total_cleared(UTF-8).txt";
	private String stopWordsPath = this.prefix + "chinese_stopword.txt";
	// private String punctuationPath = this.prefix + "punctuation.txt";
	private int scale = 10;
	private String onTopicPres = "onTopicPresent";
	private String onTopicAbs = "onTopicAbsent";
	private String offTopicPres = "offTopicPresent";
	private String offTopicAbs = "offTopicAbsent";

	public TextMiningHandler() throws IOException {
		this.segmenter = new JiebaSegmenter();
		this.wordDictionary = WordDictionary.getInstance();
		this.wordDictionary.loadUserDict(Paths.get(this.monDictPath), Charsets.UTF_8);
		// this.punctuations = Files.readAllLines(Paths.get(this.punctuationPath),
		// Charsets.UTF_8);
		this.stopWords = Files.readAllLines(Paths.get(this.stopWordsPath), Charsets.UTF_8);
	}

	public static TextMiningHandler getInstance() throws IOException {
		return new TextMiningHandler();
	}

	public boolean isSameRows(String tableName) throws ClassNotFoundException, SQLException {
		int firstRowCount = DatabaseHandler.getRowCount(tableName, "record");
		int secondRowCount = DatabaseHandler.getRowCount(tableName, "rowData");
		return firstRowCount == secondRowCount;
	}

	public List<String> getKeywords(String sentence, int topNum, String option)
			throws ClassNotFoundException, SQLException {
		Map<String, Map<String, Integer>> pmiInfo = this.getPmiInfo(sentence, option);
		List<Entry<String, BigDecimal>> pmis = this.countPmi(pmiInfo);

		List<String> pmiRank = pmis.parallelStream().map(item -> item.getKey()).collect(Collectors.toList()).subList(0,
				topNum);
		logger.info("取得前{}個PMI關鍵字: {}", topNum, pmiRank);

		return pmiRank;
	}

	public Map<String, Map<String, Integer>> getPmiInfo(String sentence, String option)
			throws ClassNotFoundException, SQLException {
		ResultSet resultSet = DatabaseHandler.getAllData("question");
		List<String> totalSegResult = Lists.newArrayList();
		List<List<String>> offTopics = Lists.newArrayList();
		while (resultSet.next()) {
			String question = resultSet.getString("question");
			List<String> offTopic = this.cutAndFilter(question, option);
			if (!offTopic.isEmpty()) {
				offTopics.add(offTopic);
				totalSegResult.addAll(offTopic);
				logger.info("正在加入offTopics: {}", offTopic);
			}
		}
		List<String> tempSegs = Arrays.asList(sentence.split("，"));
		List<String> segResult = Lists.newArrayList();
		tempSegs.stream().forEach(item -> {
			segResult.addAll(this.cutAndFilter(item, option).stream().distinct().collect(Collectors.toList()));
		});
		totalSegResult.addAll(segResult);
		List<String> totalSegDistinctResult = totalSegResult.stream().distinct().collect(Collectors.toList());
		Map<String, Map<String, Integer>> pmiInfo = Maps.newHashMap();

		// On topic
		totalSegDistinctResult.parallelStream().forEach(singleDistinctSeg -> {
			Map<String, Integer> info = Maps.newHashMap();
			if (segResult.indexOf(singleDistinctSeg) != -1) {
				if (!info.containsKey(this.onTopicPres)) {
					info.put(this.onTopicPres, 1);
				} else {
					int count = info.get(this.onTopicPres);
					info.put(this.onTopicPres, count + 1);
				}
				logger.info("正在計算term: {}, {}, {}", singleDistinctSeg, this.onTopicPres, info.get(this.onTopicPres));
			} else {
				if (!info.containsKey(this.onTopicAbs)) {
					info.put(this.onTopicAbs, 1);
				} else {
					int count = info.get(this.onTopicAbs);
					info.put(this.onTopicAbs, count + 1);
				}
				logger.info("正在計算term: {}, {}, {}", singleDistinctSeg, this.onTopicAbs, info.get(this.onTopicAbs));
			}
			if (!pmiInfo.containsKey(singleDistinctSeg)) {
				synchronized (pmiInfo) {
					pmiInfo.put(singleDistinctSeg, info);
				}
			} else {
				synchronized (pmiInfo) {
					Map<String, Integer> singleInfo = pmiInfo.get(singleDistinctSeg);
					singleInfo.putAll(info);
					pmiInfo.put(singleDistinctSeg, info);
				}
			}
			logger.info("正在加入PmiInfo -> term: {}, {}", singleDistinctSeg, pmiInfo.get(singleDistinctSeg));
		});

		// Off topic
		segResult.parallelStream().forEach(singleDistinctSeg -> {
			Map<String, Integer> info = Maps.newHashMap();
			for (List<String> offTopic : offTopics) {
				if (offTopic.indexOf(singleDistinctSeg) != -1) {
					if (!info.containsKey(this.offTopicPres)) {
						info.put(this.offTopicPres, 1);
					} else {
						int count = info.get(this.offTopicPres);
						info.put(this.offTopicPres, count + 1);
					}
					logger.info("正在計算term: {}, {}, {}", singleDistinctSeg, this.offTopicPres,
							info.get(this.offTopicPres));
				} else {
					if (!info.containsKey(this.offTopicAbs)) {
						info.put(this.offTopicAbs, 1);
					} else {
						int count = info.get(this.offTopicAbs);
						info.put(this.offTopicAbs, count + 1);
					}
					logger.info("正在計算term: {}, {}, {}", singleDistinctSeg, this.offTopicAbs,
							info.get(this.offTopicAbs));
				}
			}
			if (!pmiInfo.containsKey(singleDistinctSeg)) {
				synchronized (pmiInfo) {
					pmiInfo.put(singleDistinctSeg, info);
				}
			} else {
				synchronized (pmiInfo) {
					Map<String, Integer> singleInfo = pmiInfo.get(singleDistinctSeg);
					singleInfo.putAll(info);
					pmiInfo.put(singleDistinctSeg, singleInfo);
				}
			}
			logger.info("正在加入PmiInfo -> term: {}, {}", singleDistinctSeg, pmiInfo.get(singleDistinctSeg));
		});
		return pmiInfo;
	}

	public List<Entry<String, BigDecimal>> countPmi(Map<String, Map<String, Integer>> pmiInfo) {
		Map<String, BigDecimal> pmi = Maps.newHashMap();
		pmiInfo.entrySet().parallelStream().forEach(pmiEntry -> {
			String term = pmiEntry.getKey();
			Map<String, Integer> info = pmiEntry.getValue();
			BigDecimal onTopicPres = BigDecimal
					.valueOf(Optional.ofNullable(info.get(this.onTopicPres)).orElse(0).doubleValue());
			BigDecimal onTopicAbs = BigDecimal
					.valueOf(Optional.ofNullable(info.get(this.onTopicAbs)).orElse(0).doubleValue());
			BigDecimal offTopicPres = BigDecimal
					.valueOf(Optional.ofNullable(info.get(this.offTopicPres)).orElse(0).doubleValue());
			BigDecimal offTopicAbs = BigDecimal
					.valueOf(Optional.ofNullable(info.get(this.offTopicAbs)).orElse(0).doubleValue());
			BigDecimal total = onTopicPres.add(onTopicAbs).add(offTopicPres).add(offTopicAbs);
			logger.info("Term: {}, total: {}, onTopicPres: {}, onTopicAbs: {}, offTopicPres: {}, offTopicAbs:{}", term,
					total, onTopicPres, onTopicAbs, offTopicPres, offTopicAbs);

			BigDecimal jointProb = onTopicPres.divide(total, this.scale, RoundingMode.HALF_UP);
			BigDecimal presProb = onTopicPres.add(offTopicPres).divide(total, this.scale, RoundingMode.HALF_UP);
			BigDecimal onTopicProb = onTopicPres.add(onTopicAbs).divide(total, this.scale, RoundingMode.HALF_UP);
			logger.info("Term: {}, jointProb: {}, presProb: {}, onTopicProb: {}", term, jointProb, presProb,
					onTopicProb);
			BigDecimal eachProb = presProb.multiply(onTopicProb);
			BigDecimal pmiValue = BigDecimal
					.valueOf(
							Double.valueOf(
									Math.log(jointProb
											.divide(eachProb.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE
													: eachProb, this.scale, RoundingMode.HALF_UP)
											.doubleValue() / Math.log(2)))
									.longValue(),
							this.scale);
			synchronized (pmi) {
				pmi.put(term, pmiValue);
			}
		});
		List<Entry<String, BigDecimal>> pmis = Lists.newArrayList(pmi.entrySet());
		Collections.sort(pmis,
				(comparingEntry, comparedEntry) -> comparingEntry.getValue().compareTo(comparedEntry.getValue()));

		return Lists.reverse(pmis);
	}

	public Map<String, TfIdf> getTfIdf() throws ClassNotFoundException, SQLException {
		ResultSet resultSet = DatabaseHandler.getAllData("question");
		Map<String, TfIdf> medianWordCounts = Maps.newHashMap();
		Map<UUID, Map<String, Integer>> globalDocWordCount = Maps.newHashMap();
		Map<UUID, Map<String, BigDecimal>> globalDocTermFeq = Maps.newHashMap();
		List<String> questions = Lists.newArrayList();
		List<List<String>> segResults = Lists.newArrayList();
		while (resultSet.next()) {
			UUID uuid = UUID.fromString(resultSet.getString("uuid"));
			String question = resultSet.getString("question");
			questions.add(question);

			List<String> segResult = Lists.newArrayList();
			for (String part : Arrays.asList(question.split("，"))) {
				segResult.addAll(this.segmenter.process(part, SegMode.SEARCH).stream().map(item -> item.word)
						.collect(Collectors.toList()));
			}

			segResult = segResult.stream().map(item -> item.replaceAll("\\pP|\\w+", "")).filter(item -> !item.isEmpty())
					.collect(Collectors.toList());

			segResults.add(segResult);

			Map<String, Integer> localWordCount = this.getWordCount(segResult);

			Map<String, BigDecimal> localDocTermFeq = Maps.newHashMap();
			for (String term : segResult) {
				BigDecimal wordCount = BigDecimal.valueOf(Integer.valueOf(localWordCount.get(term)).doubleValue());
				BigDecimal termReq = wordCount.divide(
						BigDecimal.valueOf(Integer.valueOf(segResult.size()).doubleValue()), this.scale,
						RoundingMode.HALF_UP);
				localDocTermFeq.put(term, termReq);
				logger.info("Term: {}, TermFeq: {}", term, localDocTermFeq.get(term));
			}

			globalDocWordCount.put(uuid, localWordCount);
			globalDocTermFeq.put(uuid, localDocTermFeq);
		}

		Set<String> termsSet = Sets.newHashSet();
		for (Entry<UUID, Map<String, Integer>> entry : globalDocWordCount.entrySet()) {
			for (String key : entry.getValue().keySet()) {
				termsSet.add(key);
			}
		}
		List<String> terms = Lists.newArrayList(termsSet);
		for (String term : terms) {
			List<Integer> wordCounts = Lists.newArrayList();
			List<BigDecimal> termFreqs = Lists.newArrayList();
			for (Entry<UUID, Map<String, Integer>> entry : globalDocWordCount.entrySet()) {
				Optional<Integer> wordCountOptional = Optional.ofNullable(entry.getValue().get(term));
				Optional<BigDecimal> termFreqOptional = Optional
						.ofNullable(globalDocTermFeq.get(entry.getKey()).get(term));
				if (wordCountOptional.isPresent()) {
					wordCounts.add(wordCountOptional.get());
					logger.info("蒐集WordCount: {}", wordCountOptional.get(), termFreqOptional.get());
				}
				if (termFreqOptional.isPresent()) {
					termFreqs.add(termFreqOptional.get());
					logger.info("蒐集TermFeq: {}", wordCountOptional.get(), termFreqOptional.get());
				}
			}
			Collections.sort(wordCounts);
			Collections.sort(termFreqs);

			int commonIndex = Long.valueOf(Math.round(Integer.valueOf(wordCounts.size()).doubleValue() / 2)).intValue()
					- 1;

			int targetWordCount = 0;
			BigDecimal targetTermFeq = null;
			if (wordCounts.size() % 2 == 0) {
				targetWordCount = Long.valueOf(Math.round(
						Integer.valueOf((wordCounts.get(commonIndex) + wordCounts.get(commonIndex + 1))).doubleValue()
								/ 2))
						.intValue();
				targetTermFeq = termFreqs.get(commonIndex).add(termFreqs.get(commonIndex + 1))
						.divide(BigDecimal.valueOf(Integer.valueOf(2).doubleValue()), this.scale, RoundingMode.HALF_UP);
			} else {
				targetWordCount = Integer.valueOf(wordCounts.get(commonIndex));
				targetTermFeq = termFreqs.get(commonIndex);
			}

			Map<String, Integer> singleTermDocFreq = Maps.newHashMap();

			for (List<String> segResult : segResults) {
				if (segResult.indexOf(term) != -1) {
					if (!singleTermDocFreq.containsKey(term)) {
						singleTermDocFreq.put(term, 1);
					} else {
						int docFreq = singleTermDocFreq.get(term);
						singleTermDocFreq.put(term, docFreq + 1);
					}
				}
			}
			int targetDocFreq = singleTermDocFreq.get(term);
			BigDecimal bigDocFreq = BigDecimal.valueOf(Integer.valueOf(targetDocFreq).doubleValue());
			BigDecimal questionSize = BigDecimal.valueOf(Integer.valueOf(questions.size()).doubleValue());
			BigDecimal targetInverseDocFreq = BigDecimal
					.valueOf(Math.log10(questionSize.divide(bigDocFreq, scale, RoundingMode.HALF_UP).doubleValue()));
			BigDecimal targetTfIdfValue = targetTermFeq.multiply(targetInverseDocFreq);

			TfIdf tfIdf = new TfIdf(term, targetWordCount, targetTermFeq, targetDocFreq, targetInverseDocFreq,
					targetTfIdfValue, 0);
			if (this.stopWords.indexOf(term) != -1) {
				tfIdf.setIsStopWord(1);
			}
			logger.info("{}", tfIdf);
			medianWordCounts.put(term, tfIdf);
		}
		return medianWordCounts;
	}

	public Map<String, Integer> getWordCount(List<String> segResult) {
		Map<String, Integer> wordCountMap = Maps.newHashMap();
		for (String term : segResult) {
			if (!wordCountMap.containsKey(term)) {
				wordCountMap.put(term, 1);
			} else {
				int wordCount = wordCountMap.get(term);
				wordCountMap.put(term, wordCount + 1);
			}
			logger.info("Term: {}, WordCount: {}", term, wordCountMap.get(term));
		}
		return wordCountMap;
	}

	public List<String> cutAndFilter(String sentence, String option) {
		switch (option.toUpperCase()) {
		case "PTT":
			return this.segmenter.process(sentence, SegMode.SEARCH).stream()
					.map(item -> item.word.replaceAll("\\pP", ""))
					.filter(item -> !item.isEmpty() && this.stopWords.indexOf(item) == -1).collect(Collectors.toList());
		default:
			return this.segmenter.process(sentence, SegMode.SEARCH).stream()
					.map(item -> item.word.replaceAll("\\pP|\\w+", ""))
					.filter(item -> !item.isEmpty() && this.stopWords.indexOf(item) == -1).collect(Collectors.toList());
		}
	}
}
