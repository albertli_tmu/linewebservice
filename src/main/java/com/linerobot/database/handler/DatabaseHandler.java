package com.linerobot.database.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.linerobot.database.resource.DBReousrce;
import com.linerobot.textmining.tfidf.TfIdf;

public class DatabaseHandler {

	private static Logger logger = LoggerFactory.getLogger(DatabaseHandler.class);

	@SuppressWarnings("resource")
	public static void inertIndexData(UUID uuid, String question, String answer)
			throws ClassNotFoundException, SQLException {
		Connection connection = DBReousrce.getInstance();
		PreparedStatement statement = null;
		String sqlStr = "INSERT INTO " + "project.%s (uuid, %s) VALUES (?, ?)";
		try {
			String questionSqlStr = String.format(sqlStr, "question", "question");
			statement = connection.prepareStatement(questionSqlStr);
			statement.setString(1, uuid.toString());
			statement.setString(2, question);
			statement.execute();
			logger.info(questionSqlStr.replace("?", "{}"), uuid.toString(), question);

			String answerSqlStr = String.format(sqlStr, "answer", "answer");
			statement = connection.prepareStatement(answerSqlStr);
			statement.setString(1, uuid.toString());
			Optional<String> ansOptional = Optional.ofNullable(answer);
			if (ansOptional.isPresent()) {
				statement.setString(2, ansOptional.get());
			} else {
				statement.setString(2, "");
			}
			statement.execute();
			logger.info(answerSqlStr.replace("?", "{}"), uuid.toString(), answer);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			statement.close();
			connection.close();
		}
	}

	@SuppressWarnings("resource")
	public static Map<String, Map<String, String>> queryIndexData(String uuid)
			throws ClassNotFoundException, SQLException {
		Connection connection = DBReousrce.getInstance();
		PreparedStatement statement = null;
		String sqlStr = "SELECT * FROM project.%s WHERE uuid = ?";
		Map<String, Map<String, String>> resultMap = Maps.newHashMap();
		try {
			String questionSqlStr = String.format(sqlStr, "question");
			statement = connection.prepareStatement(questionSqlStr);
			statement.setString(1, uuid);
			ResultSet result = statement.executeQuery();
			logger.info(questionSqlStr.replace("?", "{}"), uuid);
			Map<String, String> questAnsMap = Maps.newHashMap();
			while (result.next()) {
				String question = result.getString("question");
				questAnsMap.put("question", question);
			}

			String answerSqlStr = String.format(sqlStr, "answer");
			statement = connection.prepareStatement(answerSqlStr);
			statement.setString(1, uuid);
			result = statement.executeQuery();
			logger.info(answerSqlStr.replace("?", "{}"), uuid);
			while (result.next()) {
				String answer = result.getString("answer");
				questAnsMap.put("answer", answer);
			}
			resultMap.put(uuid, questAnsMap);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			statement.close();
			connection.close();
		}
		return resultMap;
	}

	public static ResultSet getAllData(String tableName) throws ClassNotFoundException, SQLException {
		Connection connection = DBReousrce.getInstance();
		String sqlStr = String.format("SELECT * FROM project.%s", tableName);
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sqlStr);
			resultSet = statement.executeQuery();
			logger.info(sqlStr);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			statement.close();
			connection.close();
		}
		return resultSet;
	}

	public static int getRowCount(String tableName, String queryMode) throws ClassNotFoundException, SQLException {
		Connection connection = DBReousrce.getInstance();
		String sqlStr = "";
		if (queryMode.toUpperCase().equals("RECORD")) {
			sqlStr = "SELECT * FROM project.tableRecord WHERE tableName = ? ORDER BY rowCount DESC LIMIT 1";
		} else if (queryMode.toUpperCase().equals("RAWDATA")) {
			sqlStr = String.format("SELECT MAX(serialNumber) AS rowCount FROM project.%s", tableName);
		}
		PreparedStatement statement = null;
		int rowCount = 0;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setString(1, tableName);
			ResultSet resultSet = statement.executeQuery();
			logger.info(sqlStr.replace("?", "{}"), tableName);
			while (resultSet.next()) {
				rowCount = resultSet.getInt("rowCount");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rowCount = 0;
		} finally {
			statement.close();
			connection.close();
		}
		return rowCount;
	}

	public static void insertTfIdf(TfIdf tfIdf) throws ClassNotFoundException, SQLException {
		Connection connection = DBReousrce.getInstance();
		String sqlStr = "INSERT INTO project.tfIdf (term, wordCount, tf, df, idf, tfIdf, isStopWord) VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sqlStr);
			statement.setString(1, tfIdf.getTerm());
			statement.setInt(2, tfIdf.getWordCount());
			statement.setBigDecimal(3, tfIdf.getTf());
			statement.setInt(4, tfIdf.getDf());
			statement.setBigDecimal(5, tfIdf.getIdf());
			statement.setBigDecimal(6, tfIdf.getTfIdf());
			statement.setInt(7, tfIdf.getIsStopWord());
			statement.execute();
			logger.info(sqlStr.replace("?", "{}"), tfIdf.getTerm(), tfIdf.getWordCount(), tfIdf.getTf(), tfIdf.getDf(),
					tfIdf.getIdf(), tfIdf.getTfIdf(), tfIdf.getIsStopWord());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			statement.close();
			connection.close();
		}
	}

}
