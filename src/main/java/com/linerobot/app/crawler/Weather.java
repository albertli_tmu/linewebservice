package com.linerobot.app.crawler;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.linerobot.app.crawler.resource.CrawlerResource;

import okhttp3.Request;
import okhttp3.Response;

public class Weather {

	private static Logger logger = LoggerFactory.getLogger(Astrology.class);
	private String todayUrl = "https://www.cwb.gov.tw/V7/forecast/f_index.htm";
	private String tomorrowUrl = "https://www.cwb.gov.tw/V7/forecast/f_index2.htm";

	public Weather() {

	}

	public static Weather getInstance() {
		return new Weather();
	}

	public Map<String, Map<String, String>> getAllWeatherInfo(String url) throws IOException {
		Request request = CrawlerResource.getRequest(url);
		Response response = CrawlerResource.getResponse(request);
		Document document = Jsoup.parse(response.body().string());
		String current = document.select("a.current").text();
		String[] modifyDatetimeSplit = document.select("div.modifyedDatethree").text().replace("：", ":").split(" ");
		String modifyDatetime = Joiner.on("\n").join(
				modifyDatetimeSplit[0] + ":" + Joiner.on(" ").join(modifyDatetimeSplit[2], modifyDatetimeSplit[3]),
				modifyDatetimeSplit[4]);
		Elements elements = document.select("tr");
		Map<String, Map<String, String>> weatherInfo = Maps.newHashMap();
		for (Element element : elements) {
			String city = "";
			int counter = 0;
			Map<String, String> info = Maps.newHashMap();
			for (Element td : element.select("td")) {
				switch (counter) {
				case 0:
					city = td.text().replace("臺", "台");
					break;
				case 1:
					info.put("temp", td.text());
					break;
				case 2:
					info.put("rainyProb", td.text());
					break;
				case 3:
					info.put("weatherType", td.select("img").attr("title"));
					info.put("current", current);
					info.put("modifyDatetime", modifyDatetime);
					weatherInfo.put(city, info);
					logger.info("正在加入預報時間: {},縣市: {}, 氣象預報: {}", current, city, info);
					city = "";
					counter = 0;
					break;
				}
				counter++;
			}
		}
		return weatherInfo;
	}

	public String getSearchWeather(String searchCity) throws IOException {
		Map<String, Map<String, String>> todayWeather = this.getAllWeatherInfo(this.todayUrl);
		Map<String, Map<String, String>> tomorrowWeather = this.getAllWeatherInfo(this.tomorrowUrl);
		String fixedInput = searchCity.replace("臺", "台");
		for (Entry<String, Map<String, String>> entry : todayWeather.entrySet()) {
			String city = entry.getKey();
			if (city.matches(".*" + fixedInput + ".*")) {
				Map<String, String> todayInfo = entry.getValue();
				Map<String, String> tomorrowInfo = tomorrowWeather.get(city);
				String todayInfoStr = Joiner.on("\n").join(todayInfo.get("current"), "溫度:" + todayInfo.get("temp"),
						"降雨機率:" + todayInfo.get("rainyProb"), "天氣:" + todayInfo.get("weatherType"));
				String tomorrowInfoStr = Joiner.on("\n").join(tomorrowInfo.get("current"),
						"溫度:" + tomorrowInfo.get("temp"), "降雨機率:" + tomorrowInfo.get("rainyProb"),
						"天氣:" + tomorrowInfo.get("weatherType"));
				return Joiner.on("\n\n").join("查詢縣市:" + city + "\n" + todayInfo.get("modifyDatetime"), todayInfoStr,
						tomorrowInfoStr);
			}
		}
		return "請重新輸入完整的縣市名稱！";
	}
}
