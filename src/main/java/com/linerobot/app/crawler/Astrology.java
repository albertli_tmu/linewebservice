package com.linerobot.app.crawler;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.linerobot.app.crawler.resource.CrawlerResource;

import okhttp3.Request;
import okhttp3.Response;

public class Astrology {

	private static Logger logger = LoggerFactory.getLogger(Astrology.class);

	private String url = "https://www.daily-zodiac.com/mobile/";

	public Astrology() {

	}

	public static Astrology getInstance() {
		return new Astrology();
	}

	public Map<String, String> getAllAstrologies() throws IOException {
		Request request = CrawlerResource.getRequest(this.url);
		Response response = CrawlerResource.getResponse(request);

		Map<String, String> astrologies = Maps.newHashMap();
		Document document = Jsoup.parse(response.body().string());
		for (Element element : document.select("li")) {
			String name = element.select("p.name").text();
			List<String> splitors = Arrays.asList(element.select("a").attr("href").split("/"));
			String newHref = this.url
					+ Joiner.on("/").appendTo(new StringBuilder(), splitors.get(2), splitors.get(3)).toString();
			astrologies.put(name, newHref);
			logger.info("取得星座:{}, 目標網址:{}", name, newHref);
		}
		return astrologies;
	}

	public String getAstrologyInfo(String target) throws IOException {
		Map<String, String> astrologies = this.getAllAstrologies();
		String targetUrl = "";
		for (Entry<String, String> entry : astrologies.entrySet()) {
			if (entry.getKey().matches(".*" + target + ".*")) {
				targetUrl = entry.getValue();
				break;
			}
		}
		Request request = CrawlerResource.getRequest(targetUrl);
		Response response = CrawlerResource.getResponse(request);

		Document document = Jsoup.parse(response.body().string());
		Elements middleElements = document.select("div.middle");
		Elements nameElements = middleElements.select("div.name");
		String name = nameElements.select("p.name").text();
		logger.info("取得星座名稱:{}", name);
		
		String date = nameElements.select("p.date").text();
		logger.info("取得星座生日期間:{}", date);

		Elements textElements = middleElements.select("div.text");
		List<String> texts = Lists.newArrayList();
		for (Element element : textElements.select("li")) {
			texts.add(CharMatcher.whitespace().removeFrom(element.text()));
		}
		String text = Joiner.on(" ").join(texts);
		logger.info("取得星座今日運勢氣象:{}", text);
		
		String[] articleSplit = textElements.select("article").text().split("。");
		String article = Joiner.on("。\n").join(articleSplit) + "。";
		logger.info("取得星座今日運勢內容:{}", article);

		String content = Joiner.on("\n\n").join(name + " " + date, text, article);

		return content;
	}

}
