package com.linerobot.app.crawler.resource;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CrawlerResource {

	public static Request getRequest(String url) {
		return new Request.Builder().url(url).build();
	}

	public static Response getResponse(Request request) throws IOException {
		@SuppressWarnings("deprecation")
		OkHttpClient client = new OkHttpClient().newBuilder().sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
				.build();
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return response;
		}
		return CrawlerResource.getResponse(request);
	}

}
