package com.linerobot.app.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@RequestMapping(value = "/testla", method = RequestMethod.GET)
	public String test(@RequestParam(value = "user", required = false) String username) {
		return "Hi," + username;
	}
}
