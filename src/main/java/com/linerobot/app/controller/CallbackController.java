package com.linerobot.app.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.linerobot.app.crawler.Astrology;
import com.linerobot.app.crawler.Weather;
import com.linerobot.lucene.IndexHandler;

@LineMessageHandler
public class CallbackController {

	private List<String> astrologies = Arrays.asList("獅子,雙子,天秤,水瓶,牡羊,射手,巨蟹,處女,魔羯,金牛,天蠍,雙魚".split(","));
	private List<String> counties = Arrays.asList(
			"基隆市,台北市,新北市,桃園市,新竹市,新竹縣,苗栗縣,台中市,彰化縣,南投縣,雲林縣,嘉義市,嘉義縣,宜蘭縣,花蓮縣,台東縣,台南市,高雄市,屏東縣,連江縣,金門縣,澎湖縣".split(","));

	@EventMapping()
	public TextMessage handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
		String answer = "";
		try {
			IndexHandler handler = IndexHandler.getInstance();
			String message = event.getMessage().getText();

			boolean isMatchAstrology = message.matches(".*星座.*");
			boolean isMatchWeather = message.matches(".*(天氣|氣象).*");
			boolean isSearchAstrology = false;
			boolean isSearchWeather = false;

			if (isMatchAstrology) {
				answer = "請問你要知道什麼星座的今日運勢？";
			} else if (isMatchWeather) {
				answer = "請問你要知道哪個縣市氣象預報？\n例如：我想要知道台北市的天氣(氣象)預報";
			}

			String targetAstrology = null;
			for (String target : this.astrologies) {
				isSearchAstrology = message.matches(".*" + target + ".*");
				if (isSearchAstrology) {
					targetAstrology = target;
					break;
				}
			}

			String targetCounty = null;
			for (String target : this.counties) {
				isSearchWeather = message.replace("臺", "台").matches(".*" + target + ".*");
				if (isSearchWeather) {
					targetCounty = target;
					Weather weather = Weather.getInstance();
					answer = weather.getSearchWeather(targetCounty);
					break;
				}
			}

			if (isSearchAstrology) {
				Astrology astrology = Astrology.getInstance();
				answer = astrology.getAstrologyInfo(targetAstrology);
			}

			if (answer.isEmpty()) {
				Map<String, Map<String, String>> query = handler.queryInputSentence(message, "ptt");
				Optional<String> ansOptional = null;
				for (Entry<String, Map<String, String>> entry : query.entrySet()) {
					Map<String, String> entryValue = entry.getValue();
					ansOptional = Optional.ofNullable(entryValue.get("answer"));
				}
				if (ansOptional.isPresent()) {
					if (ansOptional.get().matches(".+\\|.*")) {
						String[] answers = ansOptional.get().split("\\|");
						Random random = new Random();
						int answerIndex = random.nextInt(answers.length);
						answer = answers[answerIndex];
					} else {
						answer = ansOptional.get();
					}
				} else {
					answer = "不好意思，可以麻煩你再說得更清楚一些嗎？";
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			answer = "我聽不懂你在說什麼……";
		}
		return new TextMessage(answer);
	}

	@EventMapping
	public void handleDefaultMessageEvent(Event event) {
		System.out.println("event: " + event);
	}

}
