package com.linerobot.app;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.linerobot.database.handler.DatabaseHandler;
import com.linerobot.lucene.IndexHandler;

public class RunDefaultIndex {

	private static Logger logger = LoggerFactory.getLogger(RunDefaultIndex.class);

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException,
			ParseException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

		while (true) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("請輸入要執行的項目：\n1.index\n2.query\n3.exit");
			String option = scanner.next();
			if (option.toUpperCase().equals("INDEX")) {
				System.out.println("請輸入要執行什麼動作：1.傳送資料到資料庫並建索引\n2.僅建立索引");
				Scanner itemScanner = new Scanner(System.in);
				String item = itemScanner.next();
				if (item.equals("1")) {
					System.out.println("請輸入要執行建索引的資料源類別：");
					Scanner optionScanner = new Scanner(System.in);
					String optional = optionScanner.next();
					if (!optional.toUpperCase().equals("PTT")) {
						optional = "";
					}
					RunDefaultIndex.indexFiles(optional);
				} else if (item.equals("2")) {
					IndexHandler handler = IndexHandler.getInstance();
					ResultSet result = DatabaseHandler.getAllData("question");
					while (result.next()) {
						String uuid = result.getString("uuid");
						String question = result.getString("question");
						handler.indexInputSentence(UUID.fromString(uuid), question, option);
						logger.info("完成建索引: UUID: {}, Question: {}", uuid, question);
					}
				}
			} else if (option.toUpperCase().equals("QUERY")) {
				System.out.println("請輸入要查詢的字串");
				Scanner sentenceScanner = new Scanner(System.in);
				String sentence = sentenceScanner.next();
				RunDefaultIndex.queryIndex(sentence, "");
			} else {
				break;
			}
		}

	}

	public static void indexFiles(String option) throws IOException, EncryptedDocumentException, InvalidFormatException,
			ClassNotFoundException, SQLException {
		List<File> files = Lists
				.newArrayList(Files.fileTraverser()
						.breadthFirst(new File(System.getProperty("user.home").replace("\\", "/") + "/Desktop/對話資料")))
				.stream().filter(item -> item.toString().endsWith(".xlsx") || item.toString().endsWith(".txt"))
				.collect(Collectors.toList());
		IndexHandler handler = IndexHandler.getInstance();

		for (File file : files) {
			logger.info("讀取檔案: {}", file.toString());
			if (file.toString().endsWith(".xlsx")) {
				Workbook workbook = WorkbookFactory.create(file);
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
				Sheet targetSheet = workbook.getSheetAt(0);

				for (int rowIndex = 1; rowIndex < targetSheet.getLastRowNum(); rowIndex++) {
					Row targetRow = targetSheet.getRow(rowIndex);
					Cell questionCell = targetRow.getCell(0);
					Cell answerCell = targetRow.getCell(1);

					CellValue questionCellValue = evaluator.evaluate(questionCell);
					CellValue answerValue = evaluator.evaluate(answerCell);

					String sentence = CharMatcher.whitespace().removeFrom(questionCellValue.getStringValue());
					String answer = CharMatcher.whitespace().removeFrom(answerValue.getStringValue());
					DatabaseHandler.inertIndexData(UUID.randomUUID(), sentence, answer);
					logger.info("完成{}列: Question -> {}, Answer -> {}", rowIndex + 1, sentence, answer);
				}
			} else if (file.toString().endsWith(".txt")) {
				List<String> lines = Files.readLines(file, Charsets.UTF_8);
				lines.parallelStream().forEach(line -> {
					String[] lineSplit = line.replace("\n", "").split("\t");
					String question = lineSplit[0];
					String answer = lineSplit[1];
					if (!answer.equals("沒有資料")) {
						try {
							DatabaseHandler.inertIndexData(UUID.randomUUID(), question, answer);
							logger.info("完成: Question -> {}, Answer -> {}", question, answer);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		}

		ResultSet result = DatabaseHandler.getAllData("question");
		while (result.next()) {
			String uuid = result.getString("uuid");
			String question = result.getString("question");
			handler.indexInputSentence(UUID.fromString(uuid), question, option);
			logger.info("完成建索引: UUID: {}, Question: {}", uuid, question);
		}

	}

	public static void queryIndex(String sentence, String option)
			throws IOException, ParseException, ClassNotFoundException, SQLException {
		IndexHandler handler = IndexHandler.getInstance();

		Map<String, Map<String, String>> result = handler.queryInputSentence(sentence, option);
		for (Entry<String, Map<String, String>> entry : result.entrySet()) {
			System.out.println(entry.getValue().get("question"));
			System.out.println(entry.getValue().get("answer"));
		}
		System.out.println(result);
	}

}
